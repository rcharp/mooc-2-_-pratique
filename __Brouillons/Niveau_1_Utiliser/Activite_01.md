# Les données au format texte

Le dossier compressé [Fiche_01.zip](../Ressources/Fiche_01.zip) contient les ressources suivantes :

- un notebook qui est une fiche élève pour manipuler les fichiers texte et les données au format csv. Une version pdf de cette fiche est disponible : [Fiche 01](../Ressources/Fiche_01/01_DonneesEnTable_FicheEleve01.pdf)
- un dossier `Images` des images du notebook
- un dossier `tables` de fichiers type tableur et csv

1. La fiche commence par annoncer le but ; mais pouvez-vous précisez les objectifs d'une telle fiche (établir un lien avec le programme officiel peut-être) ?
2. Compléter la fiche _prof_ pour cette activité
3. Quelles sont les questions de cette fiche qui semblent peu appropriées ?
4. Proposer une version corrigée de cette fiche
5. Discuter des différentes variantes de cette fiche proposées dans le cadre de ce MOOC. 